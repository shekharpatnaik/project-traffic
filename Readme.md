# Project Traffic Calculator

A question that has popped into my head multiple times last year is what are the most active non-monolith projects within `Create`. I've been thinking about it for a while and wanted to calculate "maintainer load" for our projects. I finally got around to writing an app that can calculate that.

Here is an app that calculates the number of MRs per month and the number active of maintainers on a project. It pulls down all the MRs for a project (for a year), calculates active maintainers (that have merged at least one MR) and finally draws out a few interesting charts.

I calculated stats for a few of our key projects (as of Jan 2024):

| Project                          | No of Active Maintainers | Total MRs in the year | Max MRs Merged in a month per maintainer |
|----------------------------------|--------------------------|----------------------|------------------------------------------|
| VS Code Extension                | 15                       | 603                  | 6.2                                      |
| CLI                              | 10                       | 251                  | 3.8                                      |
| GitLab Language Server           | 9                        | 203                  | 8                                        |
| GitLab Duo Plugin for JetBrains  | 8                        | 206                  | 5.38                                     |
| GitLab Plugin for Neovim         | 3                        | 85                   | 13                                       |
| GitLab Visual Studio Extension   | 7                        | 104                  | 3.86                                     |
| GitLab Workspaces Proxy          | 4                        | 52                   | 3.75                                     |
| Devfile Gem                      | 5                        | 51                   | 3.6                                      |
| gitlab-shell                     | 4                        | 219                  | 12                                       |
| Web IDE                          | 11                       | 143                  | 1.64                                     |
| Model Gateway                    | 16                       | 544                  | 5.75                                     |

The tool can be accessed [here](http://project-traffic.shekharpatnaik.co.uk/).

Please leave your feedback / feature requests here.


## Running locally.

```sh
export GITLAB_TOKEN=<YOUR PAT>

# Create a virtual environment
python3 -m venv venv

# Activate the venv
source ./venv/bin/activate

# Install dependencies
pip install -r requirements.txt

# Run the application
streamlit run main.py
```