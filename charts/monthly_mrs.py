import plotly.express as px
import streamlit as st

def _group_mrs_by_month(mrs_df):
    return mrs_df.groupby('MONTH_YEAR').size().reset_index(name='COUNT')

def show_monthly_mrs(mrs_df):
    monthly_mrs = _group_mrs_by_month(mrs_df)
    fig = px.line(monthly_mrs, x='MONTH_YEAR', y='COUNT', title='Merge Requests by Month')
    st.plotly_chart(fig, use_container_width=True)

def show_monthly_mrs_by_maintainer(mrs_df, no_of_maintainers):
    
    monthly_mrs = _group_mrs_by_month(mrs_df)
    monthly_mrs['MRs_PER_MAINTAINER'] = monthly_mrs['COUNT'] / no_of_maintainers

    fig = px.bar(monthly_mrs, x='MONTH_YEAR', y='MRs_PER_MAINTAINER', 
             title='Monthly MRs per Maintainer')

    st.plotly_chart(fig, use_container_width=True)

def show_min_and_max_mrs_per_maintainer(mrs_df, no_of_maintainers, col1, col2):
    
    monthly_mrs = _group_mrs_by_month(mrs_df)
    monthly_mrs['MRs_PER_MAINTAINER'] = monthly_mrs['COUNT'] / no_of_maintainers

    max_mrs_per_maintainer = monthly_mrs['MRs_PER_MAINTAINER'].max()
    min_mrs_per_maintainer = monthly_mrs['MRs_PER_MAINTAINER'].min()

    with col1:
        st.metric(label="Max Average MRs per Maintainer per month", value=round(max_mrs_per_maintainer, 2))

    with col2:
        st.metric(label="Min Average MRs per Maintainer per month", value=round(min_mrs_per_maintainer, 2))