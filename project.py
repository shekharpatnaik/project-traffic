from gl import get_gl
from gitlab.v4.objects import Project
import pandas as pd

def get_project_by_name(project_name: str) -> Project:
    gl = get_gl()
    project = gl.projects.get(project_name)
    return project

common_maintainers = {"stanhu", "rymai", "marin", "phikai", "francoisrose", "gl-service-secassurance-hyperproof", "dashaadu", "Service-SecurityCompliance-Authomize", "timzallmann"}

def get_maintainers(prj: Project) -> pd.DataFrame:
    members = prj.members_all.list(all=True)
    maintainer_usernames = set()
    
    rows = []
    for m in members:
        if (m.access_level == 50 or m.access_level == 40) and m.username not in common_maintainers and "bot" not in m.username:
            if m.username not in maintainer_usernames:
                rows.append([m.name, m.username, m.access_level])
                maintainer_usernames.add(m.username)

    maintainers_df = pd.DataFrame(rows, columns=["NAME", "USERNAME", "ACCESS LEVEL"])
    return maintainers_df