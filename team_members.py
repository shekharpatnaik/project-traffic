import streamlit as st
import yaml
from requests import get
import pandas as pd

TEAM_MEMBERS_URL = 'https://about.gitlab.com/company/team/team.yml'

@st.cache_data(ttl=3600)
def get_team_members(url):
    response = get(url)
    team_member_data = yaml.safe_load(response.content)
    rows = []
    for member in team_member_data:
        if 'projects' not in member:
            continue
        for (project, role) in member['projects'].items():
            if type(role) == 'list':
                for r in role:
                    if 'maintainer' not in r:
                        continue
            elif 'maintainer' not in role:
                continue

            rows.append([member["name"], member["gitlab"], project])

    df = pd.DataFrame(rows, columns=["NAME", "USERNAME", "PROJECT"])
    return df

def get_maintainers_from_team_members(project_name: str) -> pd.DataFrame:
    team_members = get_team_members(TEAM_MEMBERS_URL)
    df = team_members[team_members['PROJECT'] == project_name]
    return df

# if __name__ == '__main__':
#     members = get_team_members('https://about.gitlab.com/company/team/team.yml')
#     print(members)
    
    